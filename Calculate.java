import java.util.Arrays;

public class Calculate {
    public static int[] getCalculations() {    //1
        int x = 100;
        int y = 30;
        int sum = x + y;
        int min = x - y;
        int pr = x * y;
        int del = x / y;
        return new int[]{sum, min, pr, del};
    }

    public static void main(String[] args) {
        int[] ints = getCalculations();
        System.out.println(Arrays.toString(ints));
    }
}

